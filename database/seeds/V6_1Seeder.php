<?php

use Illuminate\Database\Seeder;

class V6_1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('settings')) {

         	DB::table('settings')->insert([
	    		[
                    'key' => 'download_video_expiry_days',
                    'value' => 3,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'key' => 'is_jwplayer_configured_mobile',
                    'value' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'key' => 'jwplayer_key_mobile',
                    'value' => '3FqL/SpvVBWLTmzbGsWMN5QGtFxz/V+KTAH2uZpHiNZTK7G2g91lMuiGeuwcZ+fR',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
			]);
    	}
    }
}
