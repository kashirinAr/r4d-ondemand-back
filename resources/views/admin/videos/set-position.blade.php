@extends('layouts.admin')

@section('title', tr('set_position'))

@section('content-header', tr('video_management'))

@section('breadcrumb')

    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i>{{ tr('home') }}</a></li>

    <li class="active"><i class="fa fa-clock-o"></i> {{ tr('set_position') }}</li>
@endsection

@section('content')

<div class="row">

    <div class="col-xs-12">

      	<div class="box box-warning">

          	<div class="box-header table-header-theme">

                <b style="font-size:18px;">@yield('title')</b>

                <a href="{{ route('admin.videos') }}" class="btn btn-default pull-right">{{ tr('view_videos') }}</a>

            </div>

            <div class="box-body">

                <div class="">

                	<h4>Guidelines:</h4>

                	<ul>
                		<li>Based on the filters, the videos will display</li>

                		<li>Set the position for each video and submit</li>

                        <li>Filter - the records will display based on the approved and no of videos greater than 0</li>
                	</ul>

                </div>

            	<hr>

                <div>

                    <form>

                    	<div class="form-group col-xs-3">

                            <label for="category_id">{{ tr('choose_category') }}</label>

                            <select class="form-control select2" multiple placeholder="{{tr('choose_category')}}">

                                @foreach($categories as $category)

                                    <option value="{{$category->id}}">{{$category->name}}</option>

                                @endforeach
                                
                            </select>
                        
                        </div>

                        <div class="form-group col-xs-3">

                            <label for="category_id">{{ tr('choose_sub_category') }}</label>

                            <select class="form-control select2" multiple>

                                @foreach($sub_categories as $sub_category)

                                    <option value="{{$sub_category->id}}">{{$sub_category->name}}</option>

                                @endforeach
                                
                            </select>
                        
                        </div>

                        <div class="form-group col-xs-3">

                            <label for="category_id">{{ tr('choose_genre') }}</label>

                            <select class="form-control select2" multiple>

                                @foreach($genres as $genre_details)

                                    <option value="{{$genre_details->id}}">{{$genre_details->name}}</option>

                                @endforeach
                                
                            </select>
                        
                        </div>

                        <div class="col-xs-3">

                            <button class="btn btn-primary">Filter</button>

                        </div>

                    </form>

                </div>

                <!-- admin videos start -->
                <div class="col-xs-12">

                    <div class="box-header"><h3>Episodes List</h3></div>

                    <div class="box-body">

                        @if(count($admin_videos) > 0)

                            <div class=" table-responsive"> 
                            
                                <form action="{{route('admin.videos')}}" method="GET" role="search">
                                    {{csrf_field()}}

                                <table id="example2" class="table table-bordered table-striped">

                                    <thead>
                                        <tr>
                                            <th>{{ tr('id') }}</th>
                                            <th>{{ tr('title') }}</th>
                                            <th>{{ tr('category') }}</th>
                                            <th>{{ tr('sub_category') }}</th>
                                            <th>{{ tr('genre') }}</th>
                                            <th>{{ tr('status') }}</th>
                                            <th>{{ tr('position') }}</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($admin_videos as $i => $admin_video_details)

                                            <tr>
                                                <td>{{ showEntries($_GET, $i+1) }}</td>

                                                <td>
                                                    <a href="{{ route('admin.view.video' , ['id' => $admin_video_details->video_id]) }}">{{ substr($admin_video_details->title , 0,25) }}...</a>
                                                </td>

                                                <td>{{ $admin_video_details->category->name ?? "-" }}</td>
                                              
                                                <td>{{ $admin_video_details->subCategory->name ?? "-" }}</td>
                                              
                                                <td>{{ $admin_video_details->genreName->name ?? '-' }}</td>

                                                <td></td>

                                                <td>

                                                    <div class="form-group">

                                                        <input type="number" name="position" class="form-control" value="{{$admin_video_details->position}}" id="position_{{$admin_video_details->admin_video_id}}">

                                                        <button class="btn btn-warning" onclick='return updateVideoPosition("{{$admin_video_details->admin_video_id}}")''>{{tr('update')}}</button>

                                                    </div>

                                                </td>

                                            </tr>

                                        @endforeach
                                    </tbody>
                                
                                </table>

                                <div align="right" id="paglink">
                                                                
                                </div>

                            </div>

                        @else 
                
                            <h3 class="no-result">{{ tr('no_video_found') }}</h3>

                        @endif
                    
                    </div>

                </div>
                <!-- admin videos end -->

            </div>

      	</div>
    
    </div>
</div>

@endsection

@section('scripts')

<script>
    function updateVideoPosition(admin_video_id) {

        var position = $('#position_'+admin_video_id).val();

        if(position <= 0) {
            alert("The position is invalid");
        }

        $.ajax({
            type : 'post',
            url : "{{route('admin.save.video.position')}}",
            data : {'admin_video_id': admin_video_id, 'position': position},
            beforeSend : function(data) {
                $("#position_btn_"+admin_video_id).attr('disabled', true);
            },
            success : function(response) {

                console.log(response);

                if (response.success) {

                    alert(response.message);

                } else {

                    alert(response.error);

                    $("#error_messages_text").html(response.error);

                }
            },
            error : function(data) {
                $("#position_btn_"+admin_video_id).attr('disabled', false);
            },
            complete : function(data) {
                $("#position_btn_"+admin_video_id).attr('disabled', false);
            }

        });

        return false;
    }
</script>

@endsection