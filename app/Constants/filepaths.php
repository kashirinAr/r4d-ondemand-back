<?php

if(!defined('PLAYER_JSON_PATH')) define('PLAYER_JSON_PATH', "/uploads/video-player-json/");

if(!defined('COMMON_IMAGE_PATH')) define('COMMON_IMAGE_PATH', "/uploads/images/");

if(!defined('SUBTITLE_PATH')) define('SUBTITLE_PATH', "/uploads/subtitles/");
